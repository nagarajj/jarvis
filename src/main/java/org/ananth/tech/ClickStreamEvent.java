package org.ananth.tech;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by ananth.durai on 1/18/16.
 */
public class ClickStreamEvent implements Serializable {

    private long value;

    public ClickStreamEvent setValue(long value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("value", value)
                .toString();
    }
}
