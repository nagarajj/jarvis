package org.ananth.tech;

import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.dsl.Disruptor;

import java.nio.ByteBuffer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by ananth.durai on 1/18/16.
 */
public class ClickStreamMain {


    public static void main(String... args) throws InterruptedException {
        final Executor executor = Executors.newCachedThreadPool();
        final ClickStreamEventFactory clickStreamEventFactory = new ClickStreamEventFactory();
        int bufferSize = 1024;
        final Disruptor<ClickStreamEvent> disruptor = new Disruptor<>(clickStreamEventFactory, bufferSize, executor);
        disruptor.handleEventsWith(new ClickStreamEventHandler());
        disruptor.start();

        final RingBuffer<ClickStreamEvent> ringBuffer = disruptor.getRingBuffer();
        final ClickStreamEventProducer clickStreamEventProducer = new ClickStreamEventProducer(ringBuffer);
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        int count = 1;
        while (true) {
            byteBuffer.putLong(0, count);
            clickStreamEventProducer.onData(byteBuffer);
            count++;
            Thread.sleep(1000);
        }

    }

}
