package org.ananth.tech;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by ananth.durai on 1/19/16.
 */
public class ClickStreamKafkaConsumer {

    KafkaConsumer<String, byte[]> consumer;
    public ClickStreamKafkaConsumer() {

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", "test");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer");
        consumer = new KafkaConsumer(props);

    }


    public void consume() {

        List<String> topicList = new ArrayList();
        topicList.add("hello");

        this.consumer.subscribe(topicList);

        System.out.println("polling started");

        while(true) {
            ConsumerRecords<String, byte[]>  records = this.consumer.poll(100);

            for(ConsumerRecord<String,byte[]> record : records) {
                ClickStreamEvent clickStreamEvent = (ClickStreamEvent) SerializationUtils.deserialize(record.value());
                System.out.println(clickStreamEvent.toString());
            }


        }


    }



    public static void main(String... args) {
        final ClickStreamKafkaConsumer clickStreamKafkaConsumer = new ClickStreamKafkaConsumer();
        clickStreamKafkaConsumer.consume();
    }

}
