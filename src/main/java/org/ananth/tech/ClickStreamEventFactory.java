package org.ananth.tech;

import com.lmax.disruptor.EventFactory;

/**
 * Created by ananth.durai on 1/15/16.
 */
public class ClickStreamEventFactory implements EventFactory<ClickStreamEvent> {


    @Override
    public ClickStreamEvent newInstance() {
        return new ClickStreamEvent();
    }
}
