package org.ananth.tech;

import com.lmax.disruptor.RingBuffer;

import java.nio.ByteBuffer;

/**
 * Created by ananth.durai on 1/18/16.
 */
public class ClickStreamEventProducer {


    private final RingBuffer<ClickStreamEvent> ringBuffer;

    public ClickStreamEventProducer(final RingBuffer<ClickStreamEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void onData(ByteBuffer byteBuffer) {
        long sequence = this.ringBuffer.next();
        try {

            final ClickStreamEvent clickStreamEvent = this.ringBuffer.get(sequence);
            clickStreamEvent.setValue(byteBuffer.getLong(0));

        } finally {
            this.ringBuffer.publish(sequence);
        }
    }



}
