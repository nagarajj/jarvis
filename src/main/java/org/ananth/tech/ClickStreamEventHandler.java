package org.ananth.tech;

import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.LifecycleAware;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

/**
 * Created by ananth.durai on 1/18/16.
 */
public class ClickStreamEventHandler implements EventHandler<ClickStreamEvent>, LifecycleAware {

    private Producer<String, byte[]> producer;
    private static final String TOPIC = "hello";

    public ClickStreamEventHandler() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
        this.producer = new KafkaProducer(props);
    }


    @Override
    public void onEvent(ClickStreamEvent clickStreamEvent, long sequence, boolean endOfBatch) throws Exception {

        System.out.println("click stream called");

        if(clickStreamEvent == null) {
            return;
        }
        byte[] data = SerializationUtils.serialize(clickStreamEvent);
        this.producer.send(new ProducerRecord(TOPIC, data));

        System.out.println("successfully written");

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onShutdown() {
        this.producer.close();
    }
}
