package com.berkeley.sparkml

import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by ananth.durai on 1/19/16.
 */
object HelloWorld {

  def main (args: Array[String]) {

    val conf = new SparkConf().setMaster("local[*]").setAppName("Hello World")

    val sc = new SparkContext(conf);

    val data = sc.textFile("/Users/ananth.durai/Desktop/Anitha_Thanam_Resume.docx", 4)


    println(data.count())

  }

}
