package com.berkeley.sparkml

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.ByteArrayDeserializer
import org.ananth.tech.ClickStreamEvent
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
 * Created by ananth.durai on 1/25/16.
 */
object ClickStreamConsumer {


  val zkQuoram = "localhost:9092"
  val group = "test"
  val topic = "hellp"
  val numOfThreads = 2;

  def main(args : Array[String]): Unit = {

    val sparkConf : SparkConf = new SparkConf().setAppName("Click Stream Spark Consumer")
    val ssc : StreamingContext = new StreamingContext(sparkConf, Seconds(2))
    val kafkaParams = Map[String, String]("metadata.broker.list" -> zkQuoram)
    val topicSet = Set[String](topic)
    val message  = KafkaUtils.createDirectStream[String,ClickStreamEvent, StringDeserializer, ByteArrayDeserializer](ssc, kafkaParams, topicSet)
    message.count().print()
    ssc.start()
    ssc.awaitTermination()


  }


}
